#include "stdafx.h"
#include "Settings.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include <vector>
#include <string>
#include <sstream>
#include <fstream>

CSettings g_Settings;

namespace
{
	struct FileReader : CSettings::IReader
	{
		FileReader(LPCTSTR fileName);;

		virtual bool ReadLine(std::string& result);
	private:
		std::ifstream file;
	};

	inline FileReader::FileReader(LPCTSTR filename)
		: file(filename, std::ios_base::in)
	{

	}

	inline bool FileReader::ReadLine(std::string& result)
	{
		std::getline(file, result);
		return file.eof();
	}

	struct FileWriter : CSettings::IWriter
	{
		FileWriter(LPCTSTR filename);
		virtual void WriteString(const std::string& source);

	private:
		std::ofstream file;
	};

	inline FileWriter::FileWriter(LPCTSTR filename)
		: file(filename, std::ios_base::out | std::ios_base::trunc)
	{

	}

	inline void FileWriter::WriteString(const std::string& source)
	{
		file << source;
	}

}


CSettings::CSettings()
{
	m_dwPollingPeriod = 1000;
	m_bTestLoopback = FALSE;
	m_bShowSIOMessages = FALSE;
	m_bShowMessageErrors = FALSE;
	m_bShowCOMErrors = FALSE;
	m_strSettingsReportPath = _T("ugs.rep");

	m_nBufferSize = 0x90000;

	m_nIncomingPort = 11112;

	m_strCOMSetup = _T("COM1: baud=9600 data=8 parity=N stop=1");
	m_iCOMRttc = 0;
	m_iCOMWttc = 0;
	m_iCOMRit = -1;

	m_arPrefix.clear();

	m_wComposedType = 0x000003;
	m_wOutputComposedType = 0x0000;
	m_wCRC16Init = 0xFFFF;

	m_wCPAddr = 0x0000;
	m_wPUAddr = 0x0000;

	m_bUnpackAll = FALSE;
	m_bMarkAll = FALSE;

	m_nStatusPeriod = 0;
	m_iSendStatTO = 1000000;
	m_StatusHdr = MESSAGETYPE(0x0000, 0x20);
	m_StatusMsg = MESSAGETYPE(m_wComposedType);
	m_MarkNestedMask = MESSAGETYPE();
	m_MarkComposedMask = MESSAGETYPE();
	m_arStatusData.clear();
	MakeStatusMsg();
	UpdateStatusMsg(0);
	m_TUType = 0x000002;
	m_TUSrcMask = 0x0000;
	m_TUSrcComMsgIndex = FALSE;
	m_TUPrimToSecSrc = 1;
	m_TUSecToPrimSrc = 1;


	m_bKeepLog = FALSE;
	m_wLogComposedType = 0x0000;
	m_bLogUnpackAll = FALSE;

	m_wLogComposedTypeToPack = 0x0000;
	m_bLogUnpackAll = FALSE;

	m_wSourceID = 0x000020;
	m_wStatusRequestMessageType = 0x0001;

	MESSAGETYPE typeStatus(0x0001, 0x1000);
	m_mapMsgTypes[0x0001] = typeStatus;
}

CSettings::~CSettings()
{
}

void replDef(unsigned short& expl, const unsigned short& def)
{
  expl = expl ? expl : def;
}

namespace
{
	struct map_lookup_decorator
	{
		map_lookup_decorator(const std::map<std::string, std::string>& origin)
			: origin_(origin)
		{

		}

		bool Lookup(const std::string& key, std::string& value)
		{
			auto it = origin_.find(key);
			if (it == origin_.end())
				return false;
			value = it->second;
			return true;
		}
	private:
		const std::map<std::string, std::string>& origin_;
	};

	std::vector<std::string> key_value_split(const std::string& source, const std::string::value_type& splitter)
	{
		std::vector<std::string> result;

		auto splitterPos = source.find_first_of(splitter);
		result.push_back(source.substr(0, splitterPos));
		result.push_back(source.substr(splitterPos+1));
		return result;
	}
	std::vector<std::string> split(const std::string& source, const std::string::value_type& splitter)
	{
		std::vector<std::string> result;
		size_t startPos = 0;
		size_t splitterPos;

		do
		{
			splitterPos = source.find_first_of(splitter, startPos);
			result.push_back(source.substr(startPos, splitterPos - startPos));
			startPos = splitterPos + 1;
		} while (splitterPos != std::string::npos);

		return result;
	}

	std::string trim(const std::string& source, std::string::value_type spacer = ' ')
	{
		size_t first = source.find_first_not_of(spacer);
		size_t second = source.find_last_not_of(spacer);
		if (first != std::string::npos)
			return source.substr(first, second - first + 1);
		return "";
	}
}
void CSettings::Load(const std::map<std::string, std::string>& mapSetting)
{
	int iValue;
	std::string strLine;
	map_lookup_decorator mapSettings(mapSetting);

	if (mapSettings.Lookup(_T("[General]/PollingPeriod"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue) && iValue != 0)
		m_dwPollingPeriod = (DWORD)iValue;

	if (mapSettings.Lookup(_T("[General]/TestLoopback"), strLine))
		m_bTestLoopback = strLine == _T("1") ? TRUE : FALSE;

	if (mapSettings.Lookup(_T("[General]/ShowSIOMessages"), strLine))
		m_bShowSIOMessages = strLine == _T("1") ? TRUE : FALSE;

	if (mapSettings.Lookup(_T("[General]/ShowMessageErrors"), strLine))
		m_bShowMessageErrors = strLine == _T("1") ? TRUE : FALSE;

	if (mapSettings.Lookup(_T("[General]/ShowCOMErrors"), strLine))
		m_bShowCOMErrors = strLine == _T("1") ? TRUE : FALSE;

	if (mapSettings.Lookup(_T("[General]/SettingsReportPath"), strLine))
		m_strSettingsReportPath = strLine;

	if (mapSettings.Lookup(_T("[General]/BufferSize"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue) && iValue != 0)
		m_nBufferSize = (UINT)iValue;

	if (mapSettings.Lookup(_T("[UDP]/IncomingPort"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue) && iValue != 0)
		m_nIncomingPort = (UINT)iValue;

	
	if (mapSettings.Lookup(_T("[UDP]/OutgoingIP"), strLine))
	{
		DWORD dwTemp = (DWORD)inet_addr(strLine.c_str());
		if (INADDR_NONE == dwTemp)
		{
			LPHOSTENT lphost;
			lphost = gethostbyname(strLine.c_str());
			if (lphost != NULL)
				dwTemp = ((LPIN_ADDR)lphost->h_addr)->s_addr;
			else
			{
				::WSASetLastError(WSAEINVAL);
				dwTemp = (DWORD)ntohl((u_long)INADDR_LOOPBACK);
			}
		}

	}


	if (mapSettings.Lookup(_T("[COM]/SetupParams"), strLine))
		m_strCOMSetup = strLine;

	if (mapSettings.Lookup(_T("[COM]/rttc"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue))
		m_iCOMRttc = iValue;

	if (mapSettings.Lookup(_T("[COM]/wttc"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue))
		m_iCOMWttc = iValue;

	if (mapSettings.Lookup(_T("[COM]/rit"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue))
		m_iCOMRit = iValue;

	std::vector<BYTE> arTemp;
	if (mapSettings.Lookup(_T("[Message]/CPAddr"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
		m_wCPAddr = (WORD)iValue;
	if (mapSettings.Lookup(_T("[Message]/PUAddr"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
		m_wPUAddr = (WORD)iValue;

	if (mapSettings.Lookup(_T("[Message]/Prefix"), strLine) && ByteArrayFromString(strLine.c_str(), arTemp, _T("")))
		m_arPrefix=arTemp;

	if (mapSettings.Lookup(_T("[Message]/OutPrefix"), strLine) && ByteArrayFromString(strLine.c_str(), arTemp, _T("")))
		m_arOutPrefix = arTemp;

	if (mapSettings.Lookup(_T("[Message]/CRC16Init"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
		m_wCRC16Init = (WORD)iValue;

	if (mapSettings.Lookup(_T("[Message]/ComposedType"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
		m_wComposedType = (WORD)iValue;

	if (mapSettings.Lookup(_T("[Message]/OutputComposedType"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
		m_wOutputComposedType = (WORD)iValue;

	if (mapSettings.Lookup(_T("[Message]/TypesToUnPack"), strLine))
	{
		m_mapMsgTypesToUnpack.clear();
		if (strLine == _T("*"))
		{
			m_mapMsgTypesToUnpack[0x0000] = NULL;
			m_bUnpackAll = TRUE;
		}
		else
		{
			auto parts = split(strLine, ' ');
			for (size_t i = 0; i < parts.size(); ++i)
			{
				if (::StrToIntEx(parts[i].c_str(), STIF_SUPPORT_HEX, &iValue))
					m_mapMsgTypesToUnpack[(WORD)iValue] = NULL;
			}
			m_bUnpackAll = FALSE;
		}
	}

	if (mapSettings.Lookup(_T("[Message]/MarkComposedMessageMask"), strLine))
	{
		int iPos = 0;
		auto parts = split(strLine, ' ');
		if (::StrToIntEx(parts[0].c_str(), STIF_SUPPORT_HEX, &iValue))
			m_MarkComposedMask.m_wDestMask = (WORD)iValue;
		if (parts.size() >=2 && ::StrToIntEx(parts[1].c_str(), STIF_SUPPORT_HEX, &iValue))
			m_MarkComposedMask.m_wSrcMask = (WORD)iValue;
	}
	if (mapSettings.Lookup(_T("[Message]/MarkMessageMask"), strLine))
	{
		int iPos = 0;
		auto parts = split(strLine, ' ');

		if (::StrToIntEx(parts[0].c_str(), STIF_SUPPORT_HEX, &iValue))
			m_MarkNestedMask.m_wDestMask = (WORD)iValue;
		if (parts.size() >= 2 && ::StrToIntEx(parts[1].c_str(), STIF_SUPPORT_HEX, &iValue))
			m_MarkNestedMask.m_wSrcMask = (WORD)iValue;
	}

	if (mapSettings.Lookup(_T("[Message]/TypesToMark"), strLine))
	{
		m_mapMsgTypesToMark.clear();
		if (strLine == _T("*"))
		{
			m_mapMsgTypesToMark[0x0000] = NULL;
			m_bMarkAll = TRUE;
		}
		else
		{
			auto parts = split(strLine, ' ');
			for (size_t i = 0; i < parts.size(); ++i)
			{
				if (::StrToIntEx(parts[i].c_str(), STIF_SUPPORT_HEX, &iValue))
					m_mapMsgTypesToMark[(WORD)iValue] = NULL;
			}
			m_bMarkAll = FALSE;
		}
	}

	m_mapMsgTypes.clear();

	for (int i = 1; i < 10; i++)
	{
		std::stringstream stream;
		stream << "[Message]/Type" << i;
		
		if (!mapSettings.Lookup(stream.str(), strLine))
			continue;

		MESSAGETYPE type;
		int iPos = 0;
		auto parts = split(strLine, ' ');
		if (::StrToIntEx(parts[0].c_str(), STIF_SUPPORT_HEX, &iValue))
			type.m_wType = (WORD)iValue;
		if (parts.size() > 1 && ::StrToIntEx(parts[1].c_str(), STIF_SUPPORT_HEX, &iValue))
			type.m_wMaxLength = (WORD)iValue;
		if (parts.size() > 2 && ::StrToIntEx(parts[2].c_str(), STIF_SUPPORT_HEX, &iValue))
			type.m_wDestination = (WORD)iValue;
		if (parts.size() > 3 && ::StrToIntEx(parts[3].c_str(), STIF_SUPPORT_HEX, &iValue))
			type.m_wSource = (WORD)iValue;
		if (parts.size() > 4 && ::StrToIntEx(parts[4].c_str(), STIF_SUPPORT_HEX, &iValue))
			type.m_wDestMask = (WORD)iValue;
		if (parts.size() > 5 && ::StrToIntEx(parts[5].c_str(), STIF_SUPPORT_HEX, &iValue))
			type.m_wSrcMask = (WORD)iValue;

		if (type.m_wType == 0x0505) {
			replDef(type.m_wSource, m_wPUAddr);
			replDef(type.m_wDestination, m_wCPAddr);
		}
		else if (type.m_wType == 0x0521 || type.m_wType == 0x0532) {
			replDef(type.m_wSource, m_wCPAddr);
		}
		else {
			replDef(type.m_wSource, m_wCPAddr);
			replDef(type.m_wDestination, m_wPUAddr);
		}

		m_mapMsgTypes[type.m_wType] = type;
	}

	MESSAGETYPE typeStatus(0x0001, 0x1000);
	m_mapMsgTypes[0x0001] = typeStatus;

	if (mapSettings.Lookup(_T("[Message]/StatusPeriod"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue))
		m_nStatusPeriod = (UINT)iValue;

	if (mapSettings.Lookup(_T("[Message]/SendStatusTO"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue))
		m_iSendStatTO = (int)iValue;

	if (mapSettings.Lookup(_T("[Message]/StatusMsg"), strLine))
	{
		int iPos = 0;
		auto parts = split(strLine, ' ');
		if (::StrToIntEx(parts[0].c_str(), STIF_SUPPORT_HEX, &iValue))
			m_StatusHdr.m_wType = (WORD)iValue;
		if (parts.size() > 1 && ::StrToIntEx(parts[1].c_str(), STIF_SUPPORT_HEX, &iValue))
			m_StatusHdr.m_wDestination = (WORD)iValue;
		if (parts.size() > 2 && ::StrToIntEx(parts[2].c_str(), STIF_SUPPORT_HEX, &iValue))
			m_StatusHdr.m_wSource = (WORD)iValue;
		if (parts.size() > 3 && ::StrToIntEx(parts[3].c_str(), STIF_SUPPORT_HEX, &iValue))
			m_StatusMsg.m_wType = (WORD)iValue;
		if (parts.size() > 4 && ::StrToIntEx(parts[4].c_str(), STIF_SUPPORT_HEX, &iValue))
			m_StatusMsg.m_wDestination = (WORD)iValue;
		if (parts.size() > 5 && ::StrToIntEx(parts[5].c_str(), STIF_SUPPORT_HEX, &iValue))
			m_StatusMsg.m_wSource = (WORD)iValue;
		if (parts.size() > 6)
		{
			ByteArrayFromString(parts[6], arTemp, _T(""));
			m_arStatusData = arTemp;
		}
		replDef(m_StatusMsg.m_wSource, m_wCPAddr);
		replDef(m_StatusMsg.m_wDestination, m_wPUAddr);

		MakeStatusMsg();
		UpdateStatusMsg(0);
	}

	if (mapSettings.Lookup(_T("[Message]/TUType"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
		m_TUType = (WORD)iValue;

	if (mapSettings.Lookup(_T("[Message]/TUSrcMask"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
		m_TUSrcMask = (WORD)iValue;
	if (mapSettings.Lookup(_T("[Message]/TUSrcComMsgIndex"), strLine))
		m_TUSrcComMsgIndex = strLine == _T("1") ? TRUE : FALSE;
	if (mapSettings.Lookup(_T("[Message]/TUPrimToSecSrc"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue))
		m_TUPrimToSecSrc = (UINT)iValue;
	if (mapSettings.Lookup(_T("[Message]/TUSecToPrimSrc"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue))
		m_TUSecToPrimSrc = (UINT)iValue;

	if (mapSettings.Lookup(_T("[Log]/KeepLog"), strLine))
		m_bKeepLog = strLine == _T("1") ? TRUE : FALSE;

	if (mapSettings.Lookup(_T("[Log]/LogIP"), strLine))
	{
		DWORD dwTemp = (DWORD)inet_addr(strLine.c_str());
		if (INADDR_NONE == dwTemp)
		{
			LPHOSTENT lphost;
			lphost = gethostbyname(strLine.c_str());
			if (lphost != NULL)
				dwTemp = ((LPIN_ADDR)lphost->h_addr)->s_addr;
			else
			{
				::WSASetLastError(WSAEINVAL);
				dwTemp = (DWORD)ntohl((u_long)INADDR_LOOPBACK);
			}
		}
	}


	if (mapSettings.Lookup(_T("[Log]/LogComposedType"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
		m_wLogComposedType = (WORD)iValue;

	if (mapSettings.Lookup(_T("[Log]/LogTypesToUnPack"), strLine))
	{
		m_mapLogMsgTypesToUnpack.clear();
		if (strLine == _T("*"))
		{
			m_mapLogMsgTypesToUnpack[0x0000] = NULL;
			m_bLogUnpackAll = TRUE;
		}
		else
		{
			auto parts = split(strLine, ' ');
			for (size_t i = 0; i < parts.size(); ++i)
			{
				if (::StrToIntEx(parts[i].c_str(), STIF_SUPPORT_HEX, &iValue))
					m_mapLogMsgTypesToUnpack[(WORD)iValue] = NULL;
			}

			m_bLogUnpackAll = FALSE;
		}
	}

	if (mapSettings.Lookup(_T("[Log]/LogComposedTypeToPack"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
		m_wLogComposedTypeToPack = (WORD)iValue;


	if (mapSettings.Lookup(_T("[Log]/LogTypesToPack"), strLine))
	{
		m_mapLogMsgTypesToPack.clear();
		if (strLine == _T("*"))
		{
			m_mapLogMsgTypesToPack[0x0000] = NULL;
			m_bLogPackAll = TRUE;
		}
		else
		{
			auto parts = split(strLine, ' ');
			for (size_t i = 0; i < parts.size(); ++i)
			{
				if (::StrToIntEx(parts[i].c_str(), STIF_SUPPORT_HEX, &iValue))
					m_mapLogMsgTypesToPack[(WORD)iValue] = NULL;
			}

			m_bLogPackAll = FALSE;
		}
	}

	if (mapSettings.Lookup(_T("[Status]/SourceIndex"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
		m_wSourceID = (WORD)iValue;

	if (mapSettings.Lookup(_T("[Status]/StatusRequestMessageType"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
		m_wStatusRequestMessageType = (WORD)iValue;

}



void CSettings::Load(CSettings::IReader& reader)
{
	std::map<std::string, std::string> mapSettings;
	std::string strGroup(_T("[General]"));

	std::string strLine;
	while (reader.ReadLine(strLine))
	{
		strLine = trim(strLine);
		size_t iComment = strLine.find_first_of(_T(';'), 0);
		if (iComment != std::string::npos)
			strLine = strLine.substr(0, iComment);

		if (strLine.empty())
			continue;

		if (strLine.find(_T('[')) == 0 && strLine.rfind(_T(']')) == strLine.length() - 1)
		{
			strGroup = strLine;
			continue;
		}

		int iPos = 0;
		auto parts = key_value_split(strLine, '=');
		std::string strKey(trim(strGroup + _T('/') + parts[0]));
		std::string strValue(parts[1]);

		mapSettings[strKey] = trim(trim(strValue), '\"');
	}
	Load(mapSettings);
}

BOOL CSettings::Load(LPCTSTR lpszProfileName)
{
	try
	{
		FileReader reader(lpszProfileName);
		Load(reader);
		return TRUE;
	}
	catch(CFileException * pfe)
	{
		pfe->Delete();
	}

	return FALSE;
}

namespace
{
	struct string_format_decorator : std::string
	{
		template<typename T>
		void Format(const char* formatString, const T& t1)
		{
			char buffer[2000];
			sprintf_s(buffer, 2000, formatString, t1);
			static_cast<std::string&>(*this) = std::string(buffer);
		}

		template<typename T1, typename T2>
		void Format(const char* formatString, const T1& t1, const T2& t2)
		{
			char buffer[2000];
			sprintf_s(buffer, 2000, formatString, t1, t2);
			static_cast<std::string&>(*this) = std::string(buffer);
		}

		template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
		void Format(const char* formatString, const T1& t1, const T2& t2, const T3& t3, const T4& t4, const T5& t5, const T6& t6)
		{
			char buffer[2000];
			sprintf_s(buffer, 2000, formatString, t1, t2, t3, t4, t5, t6);
			static_cast<std::string&>(*this) = std::string(buffer);
		}
		template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
		void Format(const char* formatString, const T1& t1, const T2& t2, const T3& t3, const T4& t4, const T5& t5, const T6& t6, const T7& t7)
		{
			char buffer[2000];
			sprintf_s(buffer, 2000, formatString, t1, t2, t3, t4, t5, t6, t7);
			static_cast<std::string&>(*this) = std::string(buffer);
		}
	};
}

void CSettings::Save(IWriter& writer)
{
	string_format_decorator strTemp;
	writer.WriteString(_T("[General]\n"));
	strTemp.Format(_T("PollingPeriod = %u\n"), m_dwPollingPeriod);
	writer.WriteString(strTemp);
	strTemp.Format(_T("TestLoopback = %s\n"), m_bTestLoopback ? _T("1") : _T("0"));
	writer.WriteString(strTemp);
	strTemp.Format(_T("ShowSIOMessages = %s\n"), m_bShowSIOMessages ? _T("1") : _T("0"));
	writer.WriteString(strTemp);
	strTemp.Format(_T("ShowMessageErrors = %s\n"), m_bShowMessageErrors ? _T("1") : _T("0"));
	writer.WriteString(strTemp);
	strTemp.Format(_T("ShowCOMErrors = %s\n"), m_bShowCOMErrors ? _T("1") : _T("0"));
	writer.WriteString(strTemp);
	strTemp.Format(_T("SettingsReportPath = \"%s\"\n"), m_strSettingsReportPath.c_str());
	writer.WriteString(strTemp);
	strTemp.Format(_T("BufferSize = 0x%04X\n"), m_nBufferSize);
	writer.WriteString(strTemp);
	writer.WriteString(_T("[UDP]\n"));
	strTemp.Format(_T("IncomingPort = %u\n"), m_nIncomingPort);
	writer.WriteString(strTemp);
	writer.WriteString(_T("[COM]\n"));
	strTemp.Format(_T("SetupParams = \"%s\"\n"), m_strCOMSetup.c_str());
	writer.WriteString(strTemp);
	strTemp.Format(_T("rttc = %i\n"), m_iCOMRttc);
	writer.WriteString(strTemp);
	strTemp.Format(_T("wttc = %i\n"), m_iCOMWttc);
	writer.WriteString(strTemp);
	strTemp.Format(_T("rit = %i\n"), m_iCOMRit);
	writer.WriteString(strTemp);
	writer.WriteString(_T("[Message]\n"));
	strTemp.Format(_T("CPAddr = 0x%04X\n"), m_wCPAddr);
	writer.WriteString(strTemp);
	strTemp.Format(_T("PUAddr = 0x%04X\n"), m_wPUAddr);
	writer.WriteString(strTemp);
	ByteArrayToString(m_arPrefix, strTemp, _T("Prefix = \""));
	writer.WriteString(strTemp + _T("\"\n"));
	strTemp.Format(_T("CRC16Init = 0x%04X\n"), m_wCRC16Init);
	writer.WriteString(strTemp);
	strTemp.Format(_T("ComposedType = 0x%04X\n"), m_wComposedType);
	writer.WriteString(strTemp);
	strTemp.Format(_T("OutputComposedType = 0x%04X\n"), m_wOutputComposedType);
	writer.WriteString(strTemp);
	writer.WriteString(_T("TypesToUnPack = \""));
	if (m_bUnpackAll)
		writer.WriteString(_T("*"));
	else
	{
		for (auto it : m_mapMsgTypesToUnpack)
		{
			strTemp.Format(_T("%04X "), it.first);
			writer.WriteString(strTemp);
		}
	}
	writer.WriteString(_T("\"\n"));

	strTemp.Format(_T("MarkComposedMessageMask  = \"0x%04X 0x%04X\"\n"), m_MarkComposedMask.m_wDestMask,
		m_MarkComposedMask.m_wSrcMask);
	writer.WriteString(strTemp);
	strTemp.Format(_T("MarkMessageMask  = \"0x%04X 0x%04X\"\n"), m_MarkNestedMask.m_wDestMask,
		m_MarkNestedMask.m_wSrcMask);
	writer.WriteString(strTemp);
	writer.WriteString(_T("TypesToMark = \""));
	if (m_bMarkAll)
		writer.WriteString(_T("*"));
	else
	{
		for (auto it : m_mapMsgTypesToMark)
		{
			strTemp.Format(_T("%04X "), it.first);
			writer.WriteString(strTemp);
		}
	}
	writer.WriteString(_T("\"\n"));

	int i = 0;
	for (auto it : m_mapMsgTypes)
	{
		WORD wType = it.first;
		MESSAGETYPE type = it.second;
		strTemp.Format(_T("Type%u = \"0x%04X 0x%X 0x%04X 0x%04X 0x%04X 0x%04X\"\n"), i++, type.m_wType,
			type.m_wMaxLength, type.m_wDestination, type.m_wSource, type.m_wDestMask, type.m_wSrcMask);
		if (i == 10)
			break;
		writer.WriteString(strTemp);
	}

	strTemp.Format(_T("StatusPeriod = %u\n"), m_nStatusPeriod);
	writer.WriteString(strTemp);
	strTemp.Format(_T("SendStatusTO = %u\n"), m_iSendStatTO);
	writer.WriteString(strTemp);
	strTemp.Format(_T("StatusMsg = \"0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X "), m_StatusHdr.m_wType,
		m_StatusHdr.m_wDestination, m_StatusHdr.m_wSource, m_StatusMsg.m_wType, m_StatusMsg.m_wDestination,
		m_StatusMsg.m_wSource);
	writer.WriteString(strTemp);
	ByteArrayToString(m_arStatusData, strTemp);
	writer.WriteString(strTemp + _T("\"\n"));

	strTemp.Format(_T("TUType = 0x%04X\n"), m_TUType);
	writer.WriteString(strTemp);

	strTemp.Format(_T("TUSrcMask = 0x%04X\n"), m_TUSrcMask);
	writer.WriteString(strTemp);
	strTemp.Format(_T("TUSrcComMsgIndex = %s\n"), m_TUSrcComMsgIndex ? _T("1") : _T("0"));
	writer.WriteString(strTemp);

	strTemp.Format(_T("TUPrimToSecSrc = %u\n"), m_TUPrimToSecSrc);
	writer.WriteString(strTemp);
	strTemp.Format(_T("TUSecToPrimSrc = %u\n"), m_TUSecToPrimSrc);
	writer.WriteString(strTemp);
	ByteArrayToString(m_arOutPrefix, strTemp, _T("OutPrefix = \""));
	writer.WriteString(strTemp + _T("\"\n"));

	writer.WriteString(_T("[Log]\n"));
	strTemp.Format(_T("KeepLog = %s\n"), m_bKeepLog ? _T("1") : _T("0"));
	writer.WriteString(strTemp);
	strTemp.Format(_T("LogComposedType = 0x%04X\n"), m_wLogComposedType);
	writer.WriteString(strTemp);
	writer.WriteString(_T("LogTypesToUnPack = \""));
	if (m_bLogUnpackAll)
		writer.WriteString(_T("*"));
	else
	{
		for (auto it : m_mapLogMsgTypesToUnpack)
		{
			strTemp.Format(_T("0x%04X "), it.first);
			writer.WriteString(strTemp);
		}
	}
	writer.WriteString(_T("\"\n"));


	strTemp.Format(_T("LogComposedTypeToPack = 0x%04X\n"), m_wLogComposedTypeToPack);
	writer.WriteString(strTemp);
	writer.WriteString(_T("LogTypesToPack = \""));
	if (m_bLogPackAll)
		writer.WriteString(_T("*"));
	else
	{
		for (auto it : m_mapLogMsgTypesToPack)
		{
			strTemp.Format(_T("0x%04X "), it.first);
			writer.WriteString(strTemp);
		}

	}
	writer.WriteString(_T("\"\n"));


	writer.WriteString(_T("[Status]\n"));

	strTemp.Format(_T("SourceIndex = 0x%04X\n"), m_wSourceID);
	writer.WriteString(strTemp);
	strTemp.Format(_T("StatusRequestMessageType = 0x%04X\n"), m_wStatusRequestMessageType);
	writer.WriteString(strTemp);
}

BOOL CSettings::Save(LPCTSTR lpszProfileName)
{
	try
	{
		FileWriter writer(lpszProfileName);
		Save(writer);
		

		return TRUE;
	}
	catch(CFileException * pfe)
	{
		pfe->Delete();
	}

	return FALSE;
}

void CSettings::MakeStatusMsg()
{
	m_StatusMsg.m_wMaxLength = 16 + (WORD)m_arStatusData.size();
	m_StatusHdr.m_wMaxLength = 16 + m_StatusMsg.m_wMaxLength;
	m_arStatusMsg.resize(m_StatusHdr.m_wMaxLength);
	BYTE * pData = m_arStatusMsg.data();
	::ZeroMemory(pData, m_StatusHdr.m_wMaxLength);
	WORD * pHeader = (WORD *)pData;
	pHeader[0] = m_StatusHdr.m_wMaxLength;
	pHeader[1] = m_StatusHdr.m_wType;
	pHeader[2] = m_StatusHdr.m_wDestination;
	pHeader[3] = m_StatusHdr.m_wSource;
	pHeader[7] = m_StatusMsg.m_wMaxLength;
	pHeader[8] = m_StatusMsg.m_wType;
	pHeader[9] = m_StatusMsg.m_wDestination;
	pHeader[10] = m_StatusMsg.m_wSource;
	memcpy(pData + 28, m_arStatusData.data(), m_arStatusData.size());
}

void CSettings::UpdateStatusMsg(unsigned char ind)
{
	BYTE * pData = m_arStatusMsg.data();
	UINT nLength = (UINT)m_arStatusMsg.size();
	*((DWORD *)(pData + 8)) = *((DWORD *)(pData + 22)) = (DWORD)time(NULL);
	pData[12] = ind;
	pData[26] = ind;
	*((WORD *)(pData + nLength - sizeof(DWORD))) = CRC16(pData + 14, nLength - 14 - sizeof(DWORD), m_wCRC16Init);
	*((WORD *)(pData + nLength - sizeof(WORD))) = CRC16(pData, nLength - sizeof(WORD), m_wCRC16Init);
}

