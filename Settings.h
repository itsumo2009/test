#ifndef _SETTINGS_H_
#define _SETTINGS_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <map>
#include <string>
#include <vector>

class CSettings
{
public:
	CSettings();
	virtual ~CSettings();

	struct IReader
	{
		virtual bool ReadLine(std::string & result) = 0;
	};
		
	struct IWriter
	{
		virtual void WriteString(const std::string & source) = 0;
	};

	
public:
	void Load(const std::map<std::string, std::string> & mapSettings);
	BOOL Load(LPCTSTR lpszProfileName);
	void Load(IReader& reader);

	void Save(IWriter& writer);
	BOOL Save(LPCTSTR lpszProfileName);
	void MakeStatusMsg();
	void UpdateStatusMsg(unsigned char);
public:
	DWORD m_dwPollingPeriod;
	BOOL m_bTestLoopback;
	BOOL m_bShowSIOMessages;
	BOOL m_bShowMessageErrors;
	BOOL m_bShowCOMErrors;
	std::string m_strSettingsReportPath;

	UINT m_nBufferSize;

	std::string m_strIncomingAddress;
	UINT m_nIncomingPort;

	std::string m_strCOMSetup;
	int m_iCOMRttc;
	int m_iCOMWttc;
	int m_iCOMRit;

	std::vector<BYTE> m_arPrefix;
	std::vector<BYTE> m_arOutPrefix;

	WORD m_wComposedType;
	WORD m_wOutputComposedType;
	WORD m_wCRC16Init;
	WORD m_wCPAddr;
	WORD m_wPUAddr;
	typedef struct tagMESSAGETYPE
	{
		WORD m_wType;			//���
		WORD m_wMaxLength;		//max ����� ��������� ��� ����
		WORD m_wDestination;	//����������
		WORD m_wSource;			//�����������
		WORD m_wDestMask;		//����� ���������� (������� ����������)
		WORD m_wSrcMask;		//����� ����������� (������� ����������)
		tagMESSAGETYPE(WORD wType = 0, WORD wMaxLength = 0, WORD wDestination = 0, WORD wSource = 0, WORD wDestMask = 0, WORD wSrcMask = 0)
		{
			m_wType = wType;
			m_wMaxLength = wMaxLength;
			m_wDestination = wDestination;
			m_wSource = wSource;
			m_wDestMask = wDestMask;
			m_wSrcMask = wSrcMask;
		}

		bool operator == (const tagMESSAGETYPE& right) const
		{
			return m_wType == right.m_wType &&
				m_wMaxLength == right.m_wMaxLength && 
				m_wDestination == right.m_wDestination &&
				m_wSource == right.m_wSource && 
				m_wDestMask == right.m_wDestMask &&
				m_wSrcMask == right.m_wSrcMask;
		}
	} MESSAGETYPE, * PMESSAGETYPE;
	std::map<WORD, MESSAGETYPE> m_mapMsgTypes;
	std::map<WORD, void*> m_mapMsgTypesToUnpack;
	BOOL m_bUnpackAll;
	std::map<WORD, void*> m_mapMsgTypesToMark;
	BOOL m_bMarkAll;

	UINT m_nStatusPeriod;
	int m_iSendStatTO;
	MESSAGETYPE m_StatusHdr;
	MESSAGETYPE m_StatusMsg;
	MESSAGETYPE m_MarkNestedMask;
	MESSAGETYPE m_MarkComposedMask;
	WORD m_TUType;
	WORD m_TUSrcMask;
	BOOL m_TUSrcComMsgIndex;
	UINT m_TUPrimToSecSrc;
	UINT m_TUSecToPrimSrc;

	std::vector<BYTE> m_arStatusData;
	std::vector<BYTE> m_arStatusMsg;

	BOOL m_bKeepLog;
	WORD m_wLogComposedType;
	std::map<WORD,void*> m_mapLogMsgTypesToUnpack;
	BOOL m_bLogUnpackAll;

	WORD m_wLogComposedTypeToPack;
	std::map<WORD,void*> m_mapLogMsgTypesToPack;
	BOOL m_bLogPackAll;
	
	//������ ugs
	WORD m_wSourceID;
	WORD m_wStatusRequestMessageType;
};

extern CSettings g_Settings;

#endif // _SETTINGS_H_
