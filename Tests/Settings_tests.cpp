#include "stdafx.h"

#include "../crc16.h"
#include "../DataRoutines.h"
#include "../Settings.h"

#include <vector>

struct TestReader : CSettings::IReader
{
	TestReader(const std::vector<const char*>& sample)
		: m_currentIndex(0)
		, m_sample(sample)
	{

	}
	// ������������ ����� IReader
	virtual bool ReadLine(std::string& result) override
	{
		if (m_currentIndex < m_sample.size())
		{
			result = CString(m_sample[m_currentIndex]);
			++m_currentIndex;
			return true;
		}
		return false;
	}
private:
	size_t m_currentIndex;
	std::vector<const char*> m_sample;
};

struct TestWriter : CSettings::IWriter
{
	TestWriter(const std::string& watchString)
		: m_watchString(watchString)
		, m_hasWatch(false)
	{

	}
	// ������������ ����� IWriter
	virtual void WriteString(const std::string& source) override
	{
		temp += source;
		if (temp.back() == '\n')
		{
			m_buffer.push_back(temp);
			temp = std::string();
		}
		if (m_buffer.back() == m_watchString)
			m_hasWatch = true;
	}

	bool hasWatch() const { return m_hasWatch; }
private:
	std::vector<std::string> m_buffer;
	std::string temp;
	std::string m_watchString;
	bool m_hasWatch;
};

TEST(CSettingsTests, CreateTest) {
   CSettings settings;
   EXPECT_EQ(settings.m_dwPollingPeriod, 1000);
	EXPECT_FALSE(settings.m_bTestLoopback);

	EXPECT_FALSE(settings.m_bShowSIOMessages);
	EXPECT_FALSE(settings.m_bShowMessageErrors);
	EXPECT_FALSE(settings.m_bShowCOMErrors);
	EXPECT_EQ(settings.m_strSettingsReportPath, _T("ugs.rep"));

	EXPECT_EQ(settings.m_nBufferSize, 0x90000);

	EXPECT_EQ(settings.m_nIncomingPort, 11112);

	EXPECT_EQ(settings.m_strCOMSetup, _T("COM1: baud=9600 data=8 parity=N stop=1"));
	EXPECT_EQ(settings.m_iCOMRttc,0);
	EXPECT_EQ(settings.m_iCOMWttc,0);
	EXPECT_EQ(settings.m_iCOMRit, -1);

	EXPECT_TRUE(settings.m_arPrefix.empty());

	EXPECT_EQ(settings.m_wComposedType, 0x000003);
	EXPECT_EQ(settings.m_wOutputComposedType,0x0000);
	EXPECT_EQ(settings.m_wCRC16Init, 0xFFFF);

	EXPECT_EQ(settings.m_wCPAddr, 0x0000);
	EXPECT_EQ(settings.m_wPUAddr, 0x0000);

	EXPECT_FALSE(settings.m_bUnpackAll);
	EXPECT_FALSE(settings.m_bMarkAll);

	EXPECT_EQ(settings.m_nStatusPeriod,0);
	EXPECT_EQ(settings.m_iSendStatTO, 1000000);
	EXPECT_EQ(settings.m_StatusHdr, CSettings::MESSAGETYPE(0x0000, 0x20));
	EXPECT_EQ(settings.m_StatusMsg, CSettings::MESSAGETYPE(settings.m_wComposedType, 16));
	EXPECT_EQ(settings.m_MarkNestedMask, CSettings::MESSAGETYPE());
	EXPECT_EQ(settings.m_MarkComposedMask, CSettings::MESSAGETYPE());
	EXPECT_TRUE(settings.m_arStatusData.empty());
	EXPECT_EQ(settings.m_TUType, 0x000002);
	EXPECT_EQ(settings.m_TUSrcMask, 0x0000);
	EXPECT_FALSE(settings.m_TUSrcComMsgIndex);
	EXPECT_EQ(settings.m_TUPrimToSecSrc, 1);
	EXPECT_EQ(settings.m_TUSecToPrimSrc, 1);


	EXPECT_FALSE(settings.m_bKeepLog);
	EXPECT_EQ(settings.m_wLogComposedType, 0x0000);
	EXPECT_FALSE(settings.m_bLogUnpackAll);

	EXPECT_EQ(settings.m_wLogComposedTypeToPack, 0x0000);
	EXPECT_FALSE(settings.m_bLogUnpackAll);

	EXPECT_EQ(settings.m_wSourceID, 0x000020);
	EXPECT_EQ(settings.m_wStatusRequestMessageType, 0x0001);

	CSettings::MESSAGETYPE typeStatus;
	EXPECT_TRUE(settings.m_mapMsgTypes.find(0x0001) != settings.m_mapMsgTypes.end());
}

TEST(CSettingsTests,PollingPeriodTest) {
	CSettings settings;
	TestReader reader({ "[General]", "PollingPeriod=123" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_dwPollingPeriod, 123);
	TestWriter writer("PollingPeriod = 123\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,TestLoopbackTest) {
	CSettings settings;
	TestReader reader({ "[General]", "TestLoopback=1" });
	settings.Load(reader);
	EXPECT_TRUE(settings.m_bTestLoopback);
	TestWriter writer("TestLoopback = 1\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,ShowSIOMessagesTest) {
	CSettings settings;
	TestReader reader({ "[General]", "ShowSIOMessages=1" });
	settings.Load(reader);
	EXPECT_TRUE(settings.m_bShowSIOMessages);
	TestWriter writer("ShowSIOMessages = 1\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,ShowMessageErrorsTest) {
	CSettings settings;
	TestReader reader({ "[General]", "ShowMessageErrors=1" });
	settings.Load(reader);
	EXPECT_TRUE(settings.m_bShowMessageErrors);
	TestWriter writer("ShowMessageErrors = 1\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,ShowCOMErrorsTest) {
	CSettings settings;
	TestReader reader({ "[General]", "ShowCOMErrors=1" });
	settings.Load(reader);
	EXPECT_TRUE(settings.m_bShowCOMErrors);
	TestWriter writer("ShowCOMErrors = 1\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,SettingsReportPathTest) {
	CSettings settings;
	TestReader reader({ "[General]", "SettingsReportPath=ugs_3.rep" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_strSettingsReportPath, "ugs_3.rep");

	TestWriter writer("SettingsReportPath = \"ugs_3.rep\"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,BufferSizeTest) {
	CSettings settings;
	TestReader reader({ "[General]", "BufferSize=0x1024" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_nBufferSize, 0x1024);

	TestWriter writer("BufferSize = 0x1024\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,IncomingPortTest) {
	CSettings settings;
	TestReader reader({ "[UDP]", "IncomingPort=1023" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_nIncomingPort, 1023);
	TestWriter writer("IncomingPort = 1023\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,COMSetupParamsTest) {
	CSettings settings;
	TestReader reader({ "[COM]", "SetupParams=COM1: baud=19200 data=8 parity=N stop=2" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_strCOMSetup, "COM1: baud=19200 data=8 parity=N stop=2");
	TestWriter writer("SetupParams = \"COM1: baud=19200 data=8 parity=N stop=2\"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,COMRitParamsTest) {
	CSettings settings;
	TestReader reader({ "[COM]", "rit=2" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_iCOMRit, 2);

	TestWriter writer("rit = 2\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,COMRttcParamsTest) {
	CSettings settings;
	TestReader reader({ "[COM]", "rttc=2" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_iCOMRttc, 2);

	TestWriter writer("rttc = 2\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,COMWttcParamsTest) {
	CSettings settings;
	TestReader reader({ "[COM]", "wttc=2" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_iCOMWttc, 2);

	TestWriter writer("wttc = 2\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,CPAddrTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "CPAddr=0x0010" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_wCPAddr, 0x0010);

	TestWriter writer("CPAddr = 0x0010\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,PUAddrTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "PUAddr=0x0010" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_wPUAddr, 0x0010);

	TestWriter writer("PUAddr = 0x0010\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,PrefixTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "Prefix=1020" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_arPrefix[0], 0x0010);
	EXPECT_EQ(settings.m_arPrefix[1], 0x0020);

	TestWriter writer("Prefix = \"1020\"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,OutPrefixTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "OutPrefix=1020" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_arOutPrefix[0], 0x0010);
	EXPECT_EQ(settings.m_arOutPrefix[1], 0x0020);

	TestWriter writer("OutPrefix = \"1020\"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,CRC16InitTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "CRC16Init=0x1020" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_wCRC16Init, 0x1020);

	TestWriter writer("CRC16Init = 0x1020\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,ComposedTypeTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "ComposedType=0x1020" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_wComposedType, 0x1020);
	TestWriter writer("ComposedType = 0x1020\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,OutputComposedTypeTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "OutputComposedType=0x1020" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_wOutputComposedType, 0x1020);

	TestWriter writer("OutputComposedType = 0x1020\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,AllTypesToUnPackTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "TypesToUnPack=*" });
	settings.Load(reader);
	EXPECT_TRUE(settings.m_bUnpackAll);
	EXPECT_TRUE(settings.m_mapMsgTypesToUnpack.find(0x0000) != settings.m_mapMsgTypesToUnpack.end());

	TestWriter writer("TypesToUnPack = \"*\"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,SomeTypesToUnPackTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "TypesToUnPack=0x0001 0x0002" });
	settings.Load(reader);
	EXPECT_FALSE(settings.m_bUnpackAll);
	EXPECT_TRUE(settings.m_mapMsgTypesToUnpack.find(0x0001) != settings.m_mapMsgTypesToUnpack.end());
	EXPECT_TRUE(settings.m_mapMsgTypesToUnpack.find(0x0002) != settings.m_mapMsgTypesToUnpack.end());

	TestWriter writer("TypesToUnPack = \"0001 0002 \"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,MarkComposedMessageMaskTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "MarkComposedMessageMask=0x0001 0x0002" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_MarkComposedMask.m_wDestMask, 0x0001);
	EXPECT_EQ(settings.m_MarkComposedMask.m_wSrcMask, 0x0002);
	
	TestWriter writer("MarkComposedMessageMask  = \"0x0001 0x0002\"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,MarkMessageMaskTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "MarkMessageMask=0x0001 0x0002" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_MarkNestedMask.m_wDestMask, 0x0001);
	EXPECT_EQ(settings.m_MarkNestedMask.m_wSrcMask, 0x0002);

	TestWriter writer("MarkMessageMask  = \"0x0001 0x0002\"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,AllTypesToMarkTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "TypesToMark=*" });
	settings.Load(reader);
	EXPECT_TRUE(settings.m_bMarkAll);
	EXPECT_TRUE(settings.m_mapMsgTypesToMark.find(0x0000) != settings.m_mapMsgTypesToMark.end());

	TestWriter writer("TypesToMark = \"*\"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,SomeTypesToMarkTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "TypesToMark=0x0001 0x0002" });
	settings.Load(reader);
	EXPECT_FALSE(settings.m_bMarkAll);
	EXPECT_TRUE(settings.m_mapMsgTypesToMark.find(0x0001) != settings.m_mapMsgTypesToMark.end());
	EXPECT_TRUE(settings.m_mapMsgTypesToMark.find(0x0002) != settings.m_mapMsgTypesToMark.end());

	TestWriter writer("TypesToMark = \"0001 0002 \"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,SimpleMessageTypeTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "Type1=0x0001 0x1000 0x0000 0x0000 0x0000 0x0000" });
	settings.Load(reader);
	CSettings::MESSAGETYPE expect;
	expect.m_wType = 0x0001;
	expect.m_wMaxLength = 0x1000;
	CSettings::MESSAGETYPE value;
	value = settings.m_mapMsgTypes[1];
	EXPECT_EQ(value, expect);

	TestWriter writer("Type0 = \"0x0001 0x1000 0x0000 0x0000 0x0000 0x0000\"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,505MessageTypeTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "Type1=0x0505 0x1000 0x0000 0x0000 0x0000 0x0000", "CPAddr=0x0010", "PUAddr=0x0010" });
	settings.Load(reader);
	CSettings::MESSAGETYPE expect;
	expect.m_wType = 0x0505;
	expect.m_wMaxLength = 0x1000;
	expect.m_wSource = settings.m_wPUAddr;
	expect.m_wDestination = settings.m_wCPAddr;
	CSettings::MESSAGETYPE value;
	value = settings.m_mapMsgTypes[0x505];
	EXPECT_EQ(value, expect);

	TestWriter writer("Type1 = \"0x0505 0x1000 0x0010 0x0010 0x0000 0x0000\"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,Message521TypeTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "Type1=0x0521 0x1000 0x0000 0x0000 0x0000 0x0000", "CPAddr=0x0010", "PUAddr=0x0010" });
	settings.Load(reader);
	CSettings::MESSAGETYPE expect;
	expect.m_wType = 0x0521;
	expect.m_wMaxLength = 0x1000;
	expect.m_wSource = settings.m_wCPAddr;
	CSettings::MESSAGETYPE value;
	value = settings.m_mapMsgTypes[0x521];
	EXPECT_EQ(value, expect);

	TestWriter writer("Type1 = \"0x0521 0x1000 0x0000 0x0010 0x0000 0x0000\"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,Message532TypeTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "Type1=0x0532 0x1000 0x0000 0x0000 0x0000 0x0000", "CPAddr=0x0010", "PUAddr=0x0010" });
	settings.Load(reader);
	CSettings::MESSAGETYPE expect;
	expect.m_wType = 0x0532;
	expect.m_wMaxLength = 0x1000;
	expect.m_wSource = settings.m_wCPAddr;
	CSettings::MESSAGETYPE value = settings.m_mapMsgTypes[0x532];
	EXPECT_EQ(value, expect);

	TestWriter writer("Type1 = \"0x0532 0x1000 0x0000 0x0010 0x0000 0x0000\"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,WithAddrMessageTypeTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "Type1=0x053 0x1000 0x0000 0x0000 0x0000 0x0000", "CPAddr=0x0010", "PUAddr=0x0010" });
	settings.Load(reader);
	CSettings::MESSAGETYPE expect;
	expect.m_wType = 0x053;
	expect.m_wMaxLength = 0x1000;
	expect.m_wSource = settings.m_wCPAddr;
	expect.m_wDestination = settings.m_wPUAddr;

	CSettings::MESSAGETYPE value = settings.m_mapMsgTypes[0x53];
	EXPECT_EQ(value, expect);

	TestWriter writer("Type1 = \"0x0053 0x1000 0x0010 0x0010 0x0000 0x0000\"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests, EnsureTypeStatusTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "Type1=0x053 0x1000 0x0000 0x0000 0x0000 0x0000", "CPAddr=0x0010", "PUAddr=0x0010" });
	settings.Load(reader);
	CSettings::MESSAGETYPE expect;
	expect.m_wType = 0x1;
	expect.m_wMaxLength = 0x1000;

	CSettings::MESSAGETYPE value = settings.m_mapMsgTypes[0x1];
	EXPECT_EQ(value, expect);
}

TEST(CSettingsTests,StatusPeriodTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "StatusPeriod=100" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_nStatusPeriod, 100);
	TestWriter writer("StatusPeriod = 100\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,SendStatusTOTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "SendStatusTO=100" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_iSendStatTO, 100);
	TestWriter writer("SendStatusTO = 100\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,StatusMsgTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "StatusMsg=0x0002 0x0003 0x0004 0x0005 0x0006 0x0007" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_StatusHdr.m_wType, 0x0002);
	EXPECT_EQ(settings.m_StatusHdr.m_wDestination, 0x0003);
	EXPECT_EQ(settings.m_StatusHdr.m_wSource, 0x0004);
	EXPECT_EQ(settings.m_StatusMsg.m_wType, 0x0005);
	EXPECT_EQ(settings.m_StatusMsg.m_wDestination, 0x0006);
	EXPECT_EQ(settings.m_StatusMsg.m_wSource, 0x0007);
	TestWriter writer("StatusMsg = \"0x0002 0x0003 0x0004 0x0005 0x0006 0x0007 \"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,TUTypeTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "TUType=0x100" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_TUType, 0x100);

	TestWriter writer("TUType = 0x0100\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,TUSrcMaskTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "TUSrcMask=0x100" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_TUSrcMask, 0x100);

	TestWriter writer("TUSrcMask = 0x0100\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,TUSrcComMsgIndexTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "TUSrcComMsgIndex=1" });
	settings.Load(reader);
	EXPECT_TRUE(settings.m_TUSrcComMsgIndex);

	TestWriter writer("TUSrcComMsgIndex = 1\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,TUPrimToSecSrcTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "TUPrimToSecSrc=1" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_TUPrimToSecSrc, 1);

	TestWriter writer("TUPrimToSecSrc = 1\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,TUSecToPrimSrcTest) {
	CSettings settings;
	TestReader reader({ "[Message]", "TUSecToPrimSrc=1" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_TUSecToPrimSrc, 1);

	TestWriter writer("TUSecToPrimSrc = 1\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,KeepLogTest) {
	CSettings settings;
	TestReader reader({ "[Log]", "KeepLog=1" });
	settings.Load(reader);
	EXPECT_TRUE(settings.m_bKeepLog);

	TestWriter writer("KeepLog = 1\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,LogComposedTypeTest) {
	CSettings settings;
	TestReader reader({ "[Log]", "LogComposedType=0x0001" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_wLogComposedType, 1);
	TestWriter writer("LogComposedType = 0x0001\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,AllTypesToLogUnPackTest) {
	CSettings settings;
	TestReader reader({ "[Log]", "LogTypesToUnPack=*" });
	settings.Load(reader);
	EXPECT_TRUE(settings.m_bLogUnpackAll);
	EXPECT_TRUE(settings.m_mapLogMsgTypesToUnpack.find(0x0000) != settings.m_mapLogMsgTypesToUnpack.end());

	TestWriter writer("LogTypesToUnPack = \"*\"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,SomeTypesToLogUnpackTest) {
	CSettings settings;
	TestReader reader({ "[Log]", "LogTypesToUnPack=0x0001 0x0002" });
	settings.Load(reader);
	EXPECT_FALSE(settings.m_bLogUnpackAll);
	EXPECT_TRUE(settings.m_mapLogMsgTypesToUnpack.find(0x0001) != settings.m_mapLogMsgTypesToUnpack.end());
	EXPECT_TRUE(settings.m_mapLogMsgTypesToUnpack.find(0x0002) != settings.m_mapLogMsgTypesToUnpack.end());

	TestWriter writer("LogTypesToUnPack = \"0x0001 0x0002 \"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,AllTypesToLogPackTest) {
	CSettings settings;
	TestReader reader({ "[Log]", "LogTypesToPack=*" });
	settings.Load(reader);
	EXPECT_TRUE(settings.m_bLogPackAll);
	EXPECT_TRUE(settings.m_mapLogMsgTypesToPack.find(0x0000) != settings.m_mapLogMsgTypesToPack.end());

	TestWriter writer("LogTypesToPack = \"*\"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,SomeTypesToLogPackTest) {
	CSettings settings;
	TestReader reader({ "[Log]", "LogTypesToPack=0x0001 0x0002" });
	settings.Load(reader);
	EXPECT_FALSE(settings.m_bLogPackAll);
	void* t;
	EXPECT_TRUE(settings.m_mapLogMsgTypesToPack.find(0x0001) != settings.m_mapLogMsgTypesToPack.end());
	EXPECT_TRUE(settings.m_mapLogMsgTypesToPack.find(0x0002) != settings.m_mapLogMsgTypesToPack.end());

	TestWriter writer("LogTypesToPack = \"0x0001 0x0002 \"\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,LogComposedTypeToPackTest) {
	CSettings settings;
	TestReader reader({ "[Log]", "LogComposedTypeToPack=1" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_wLogComposedTypeToPack, 1);

	TestWriter writer("LogComposedTypeToPack = 0x0001\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,StatusSourceIndexTest) {
	CSettings settings;
	TestReader reader({ "[Status]", "SourceIndex=1" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_wSourceID, 1);

	TestWriter writer("SourceIndex = 0x0001\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}

TEST(CSettingsTests,StatusRequestMessageTypeTest) {
	CSettings settings;
	TestReader reader({ "[Status]", "StatusRequestMessageType=1" });
	settings.Load(reader);
	EXPECT_EQ(settings.m_wStatusRequestMessageType, 1);

	TestWriter writer("StatusRequestMessageType = 0x0001\n");
	settings.Save(writer);
	EXPECT_TRUE(writer.hasWatch());
}
